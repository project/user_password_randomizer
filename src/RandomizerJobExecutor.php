<?php

declare(strict_types=1);

namespace Drupal\user_password_randomizer;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\user\UserStorageInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;

/**
 * Executes scheduled randomizer jobs.
 */
final class RandomizerJobExecutor implements LoggerAwareInterface {

  use LoggerAwareTrait;

  /**
   * @internal
   */
  public const STATE_LAST_CHECK = 'user_password_randomizer.last_check';

  protected UserStorageInterface $userStorage;

  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected StateInterface $state,
    protected TimeInterface $time,
    protected UserPasswordRandomizerInterface $randomizer,
    EntityTypeManagerInterface $entityTypeManager,
  ) {
    $this->userStorage = $entityTypeManager->getStorage('user');
  }

  /**
   * Implements hook_cron().
   *
   * Conditionally executes user metadata randomization.
   *
   * @see \user_password_randomizer_cron()
   */
  public function cron(): void {
    $config = $this->configFactory->get('user_password_randomizer.settings');

    $updateInterval = $config->get('update_interval');
    /** @var int<0, max> $updateInterval */
    $updateInterval = \is_numeric($updateInterval) ? (int) $updateInterval : 0;

    $lastCheck = $this->state->get(static::STATE_LAST_CHECK);
    /** @var int<0, max> $lastCheck */
    $lastCheck = \is_numeric($lastCheck) ? (int) $lastCheck : 0;

    $requestTime = $this->time->getRequestTime();
    if (($requestTime - $lastCheck) > $updateInterval) {
      // Skipping as update interval has not yet elapsed.
      $this->execute();
    }
  }

  /**
   * Executes randomization of randomizes user metadata.
   */
  public function execute(): void {
    // @todo allow users to be configured.
    $userId = 1;

    /** @var \Drupal\user\UserInterface $user */
    $user = $this->userStorage->load($userId);
    $loggerArgs = [
      '@user' => 'user ' . $user->id(),
    ];

    $user->setPassword($this->randomizer->generatePassword($user));

    // Don't bother if username is the same or randomization is off:
    $newUsername = $this->randomizer->generateUsername($user);
    $oldUsername = $user->getAccountName();
    if ($oldUsername !== $newUsername) {
      $loggerArgs['@old_username'] = $oldUsername;
      $loggerArgs['@new_username'] = $newUsername;
      $user->setUsername($newUsername);
      $this->logger?->info('Randomized username and password for @user. Username changed from @old_username to @new_username', $loggerArgs);
    }
    else {
      $this->logger?->info('Randomized password for @user', $loggerArgs);
    }

    try {
      $user->save();
    }
    catch (EntityStorageException $e) {
      $this->logger?->error('Failed to randomize the username and password for @user: @exception_message', $loggerArgs + [
        '@exception_message' => $e->getMessage(),
      ]);
    }

    $requestTime = $this->time->getRequestTime();
    $this->state->set(static::STATE_LAST_CHECK, $requestTime);
  }

}
