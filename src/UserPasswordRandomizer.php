<?php

declare(strict_types=1);

namespace Drupal\user_password_randomizer;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Password\PasswordGeneratorInterface;
use Drupal\Core\Utility\Token;
use Drupal\user\UserInterface;
use Drupal\user\UserStorageInterface;

/**
 * User Password Randomizer utility.
 */
class UserPasswordRandomizer implements UserPasswordRandomizerInterface {

  protected UserStorageInterface $userStorage;

  /**
   * UserPasswordRandomizer constructor.
   */
  public function __construct(
    protected Token $token,
    protected ConfigFactoryInterface $configFactory,
    EntityTypeManagerInterface $entityTypeManager,
    protected PasswordGeneratorInterface $passwordGenerator,
  ) {
    $this->userStorage = $entityTypeManager->getStorage('user');
  }

  public function generateUsername(UserInterface $user): string {
    $config = $this->configFactory->get('user_password_randomizer.settings');

    if (TRUE !== $config->get('randomize_username')) {
      return $user->getAccountName();
    }

    /** @var string $idKey */
    $idKey = $this->userStorage->getEntityType()->getKey('id');

    $countUsers = function ($username) use ($user, $idKey): int {
      return $this->userStorage->getQuery()
        ->condition('name', $username)
        // Don't include this user.
        ->condition($idKey, $user->id(), '<>')
        ->count()
        ->accessCheck(TRUE)
        ->execute();
    };

    // Generate usernames until it is unique.
    /** @var string $pattern */
    $pattern = $config->get('username_pattern');
    do {
      if ($pattern !== '') {
        $newUsername = $this->token->replace($pattern, ['user' => $user]);
      }
      else {
        // When pattern is empty we generate a random (not cryptographically
        // secure) string.
        $newUsername = $this->passwordGenerator->generate(16);
      }
    } while ($countUsers($newUsername) > 0);

    return $newUsername;
  }

  public function generatePassword(UserInterface $user): string {
    return $this->passwordGenerator->generate(16);
  }

}
