<?php

declare(strict_types=1);

namespace Drupal\Tests\user_password_randomizer\Unit {


  use Drupal\Component\Datetime\TimeInterface;
  use Drupal\Core\Entity\EntityTypeManagerInterface;
  use Drupal\Core\State\StateInterface;
  use Drupal\user\UserInterface;
  use Drupal\user\UserStorageInterface;
  use Drupal\user_password_randomizer\RandomizerJobExecutor;
  use Drupal\user_password_randomizer\UserPasswordRandomizerInterface;
  use Psr\Log\LoggerInterface;

  /**
   * @coversDefaultClass \Drupal\user_password_randomizer\RandomizerJobExecutor
   *
   * @group user_password_randomizer
   */
  class RandomizerJobExecutorTest extends RandomizerJobExecutorTestBase {

    /**
     * @covers ::execute
     */
    public function testExecute(): void {
      $time = \Mockery::mock(TimeInterface::class);
      $state = \Mockery::mock(StateInterface::class);
      $user = \Mockery::mock(UserInterface::class);
      $userStorage = \Mockery::mock(UserStorageInterface::class);
      $randomizer = \Mockery::mock(UserPasswordRandomizerInterface::class);
      $entityTypeManager = \Mockery::mock(EntityTypeManagerInterface::class);
      $logger = \Mockery::mock(LoggerInterface::class);

      /** @var \Drupal\Core\Config\ConfigFactoryInterface&\PHPUnit\Framework\MockObject\MockBuilder $configFactory */
      $configFactory = $this->getConfigFactoryStub([
        'user_password_randomizer.settings' => [
          'update_interval' => 60,
        ],
      ]);

      // 19/06/2020 @ 12:00am (UTC)
      $currentTime = 1592524800;
      $newUsername = 'abcdef123456';
      $newPassword = 'abcd1234';

      $logger->expects('info')->atLeast(1);

      $time->expects('getRequestTime')->twice()->andReturn($currentTime);

      $state->expects('get')
        ->with(RandomizerJobExecutor::STATE_LAST_CHECK)
        ->once()
        ->andReturn(NULL);
      $state->expects('set')
        ->with(RandomizerJobExecutor::STATE_LAST_CHECK, $currentTime)
        ->once();

      $randomizer->expects('generateUsername')
        ->with($user)
        ->once()
        ->andReturn($newUsername);
      $randomizer->expects('generatePassword')
        ->with($user)
        ->once()
        ->andReturn($newPassword);

      $entityTypeManager->expects('getStorage')
        ->with('user')
        ->once()
        ->andReturn($userStorage);

      $userStorage->expects('load')->with(1)->once()->andReturn($user);

      $user->expects('getAccountName')->once()->andReturn('foobar');
      $user->expects('id')->once()->andReturn(1);
      $user->expects('setUsername')->with($newUsername)->once();
      $user->expects('setPassword')->with($newPassword)->once();
      $user->expects('save')->once();

      $executor = new RandomizerJobExecutor(
        $configFactory,
        $state,
        $time,
        $randomizer,
        $entityTypeManager,
      );
      $executor->setLogger($logger);
      $executor->cron();
    }

  }
}
