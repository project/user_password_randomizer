<?php

declare(strict_types=1);

namespace Drupal\Tests\user_password_randomizer\Unit;

use Drupal\Tests\UnitTestCase;
use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Mockery\Adapter\Phpunit\MockeryTestCaseSetUp;

/**
 * Base class for unit testing.
 */
abstract class RandomizerJobExecutorTestBase extends UnitTestCase {

  use MockeryPHPUnitIntegration;
  use MockeryTestCaseSetUp;

  /**
   * Setup for MockeryTestCaseSetUp.
   */
  protected function mockeryTestSetUp(): void {
  }

  /**
   * Teardown for MockeryTestCaseSetUp.
   */
  protected function mockeryTestTearDown(): void {
  }

}
