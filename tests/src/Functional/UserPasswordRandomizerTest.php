<?php

declare(strict_types=1);

namespace Drupal\Tests\user_password_randomizer\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Basic tests class for the config forms.
 */
final class UserPasswordRandomizerTest extends BrowserTestBase {

  protected static $modules = ['user_password_randomizer'];

  protected $defaultTheme = 'stark';

  /**
   * Tests config form is visible.
   */
  public function testConfigFormPage(): void {
    $this->drupalLogin($this->rootUser);
    $this->drupalGet('/admin/config/people/user-password-randomizer');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldExists('username_pattern');
  }

}
